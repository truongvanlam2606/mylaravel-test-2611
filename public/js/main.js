// Modal
var modal = document.getElementById("myModal");
var btn = document.getElementById("cart");
var close = document.getElementsByClassName("close")[0];
var close_footer = document.getElementsByClassName("close-footer")[0];
var order = document.getElementsByClassName("order")[0];
btn.onclick = function () {
  modal.style.display = "block";
}
close.onclick = function () {
  modal.style.display = "none";
}
close_footer.onclick = function () {
  modal.style.display = "none";
}
order.onclick = function () {
  alert("Thank you for your order payment")
}
// window.onclick = function (event) {
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }


// // xóa cart
// var remove_cart = document.getElementsByClassName("btn-danger");
// for (var i = 0; i < remove_cart.length; i++) {
//   var button = remove_cart[i]
//   button.addEventListener("click", function () {
//     var button_remove = event.target
//     button_remove.parentElement.parentElement.remove()
//     updatecart()
//   })
// }
// // thay đổi số lượng
// var quantity_input = document.getElementsByClassName("cart-quantity-input");
// for (var i = 0; i < quantity_input.length; i++) {
//   var input = quantity_input[i];
//   input.addEventListener("change", function (event) {
//     var input = event.target
//     if (isNaN(input.value) || input.value <= 0) {
//       input.value = 1;
//     }
//     updatecart()
//   })
// }

// // Thêm vào giỏ
// var add_cart = document.getElementsByClassName("btn-cart");
// for (var i = 0; i < add_cart.length; i++) {
//   var add = add_cart[i];
//   add.addEventListener("click", function (event) {
//     // var button = event.target;
//     // var product = button.parentElement.parentElement.parentElement;
//     // var img = product.parentElement.getElementsByClassName("img-prd")[0].src
//     // var title = product.getElementsByClassName("content-product-h3")[0].innerText
//     // var price = product.getElementsByClassName("price")[0].innerText
//     // addItemToCart(title, price, img)
//     // modal.style.display = "block";

//     // updatecart()
//   })
// }

// function addItemToCart(title, price, img) {
//   var cartRow = document.createElement('div')
//   cartRow.classList.add('cart-row')
//   var cartItems = document.getElementsByClassName('cart-items')[0]
//   var cart_title = cartItems.getElementsByClassName('cart-item-title')
//   for (var i = 0; i < cart_title.length; i++) {
//     if (cart_title[i].innerText == title) {
//       alert('Products In Stock')
//       return
//     }
//   }

//   var cartRowContents = `
//   <div class="cart-item cart-column">
//       <img class="cart-item-image" src="${img}" width="100" height="100">
//       <span class="cart-item-title">${title}</span>
//   </div>
//   <span class="cart-price cart-column">${price}</span>
//   <div class="cart-quantity cart-column">
//       <input class="cart-quantity-input" type="number" value="1">
//       <button class="btn btn-danger" type="button">Remove</button>
//   </div>`
//   cartRow.innerHTML = cartRowContents
//   cartItems.append(cartRow)
//   cartRow.getElementsByClassName('btn-danger')[0].addEventListener('click', function () {
//     var button_remove = event.target
//     button_remove.parentElement.parentElement.remove()
//     updatecart()
//   })
//   cartRow.getElementsByClassName('cart-quantity-input')[0].addEventListener('change', function (event) {
//     var input = event.target
//     if (isNaN(input.value) || input.value <= 0) {
//       input.value = 1;
//     }
//     updatecart()
//   })
// }
// // update cart
// function updatecart() {
//   var cart_item = document.getElementsByClassName("cart-items")[0];
//   var cart_rows = cart_item.getElementsByClassName("cart-row");
//   var total = 0;
//   for (var i = 0; i < cart_rows.length; i++) {
//     var cart_row = cart_rows[i]
//     var price_item = cart_row.getElementsByClassName("cart-price ")[0]
//     var quantity_item = cart_row.getElementsByClassName("cart-quantity-input")[0]
//     var price = parseFloat(price_item.innerText)
//     var quantity = quantity_item.value
//       total = total + (price * quantity)
//   }
//   document.getElementsByClassName("cart-total-price")[0].innerText = total + ' USD'
// }



function loadCart(cart){

    if(cart){
        var contentLoad = ' ';
        var cartTotal = 0;
       if(typeof cart === 'object'){
        cart = Object.keys(cart).map(function(key){
            return cart[key]
          });
       }
       cart.forEach(function(cartItem){
            contentLoad += `
                            <div class="cart-row">
                            <div class="cart-item cart-column">
                                <img class="cart-item-image" src="${cartItem.image}" width="100" height="100">
                                <span class="cart-item-title">${cartItem.name}</span>
                            </div>
                            <span class="cart-price cart-column">${cartItem.price}</span>
                            <div class="cart-quantity cart-column">
                                <input class="cart-quantity-input" disabled type="number" value="${cartItem.qty}">
                                <button class="btn btn-danger removeProductToCart" data-productId="${cartItem.product_id}" type="button">Remove</button>
                            </div>
                            </div>`
            cartTotal += parseFloat(cartItem.price) * parseInt(cartItem.qty);
        })

        $('#load-cart').html(contentLoad);
        $('#loadCartTotal').text( cartTotal.toString() +' USD');
        $('#loadCartQty').text( cart.length);
    }

}

 jQuery(document).on('click' ,'.removeProductToCart' ,function(){
    event.preventDefault();
    var productId = $(this).data('productid');
    console.log('removeProductToCart:' +productId)
    var url = '/cart/remove-product-to-cart/' + productId;
    $.ajax({
        url: url,
        type:"GET",
        success:function(response){
          if(response.error){
            alert(response.message)
          }else{
            alert('Remove Cart Item Success !')
            console.log(response)
            loadCart(response.cart)
          }

        },
        error: function(error) {
            alert(error)
        }
       });

})

jQuery(document).ready(function($){

    $('.slick-images').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows : false,
    });

    jQuery(".addProductToCart").click(function(){
        event.preventDefault();
        var productId = $(this).data('productid');

        console.log(productId)
        var url = '/cart/add-product-to-cart/' + productId;
        $.ajax({
            url: url,
            type:"GET",
            success:function(response){
              if(response.error){
                alert(response.message)
              }else{
                alert('Add Cart Success !')
                console.log(response)
                loadCart(response.cart)
              }

            },
            error: function(error) {
                alert(error)
            }
           });
    })


    jQuery("#cart").click(function(){

        $.ajax({
            url: '/cart/get-cart',
            type:"GET",
            success:function(response){
              if(response.error){
                alert(response.message)
              }else{
                console.log(response)
                console.log(response.cart)
                loadCart(response.cart)
              }

            },
            error: function(error) {
                alert(error)
            }
           });
    })

});
