@extends('admin.layouts.layout')
@section('title', 'List Products')

@section('content')
    <div class="content-wrapper" style="min-height: calc(100vh - 200px);margin-bottom:-30px">
        <section class="content-header">
            <h1>
                {{ __('Simple Posts') }}
                <small>{{ __('preview of simple posts') }}</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dashboard') }}"><i class="fa fa-dashboard"></i>{{ __('Dashboard') }}</a></li>
                <li><a href="#" class="active">{{ __('Posts') }}</a></li>
            </ol>
        </section>
        <div class="row nav-tabs">
            <div class="col-sm-6">
                <ul class="nav display-flex" id="myTab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link"  href="{{ route('admin.product.index') }}">{{ __('Lists Of Post') }}</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 justify-content-end display-flex ml--15">
                <a href="{{route('admin.product.create')}}" class="btn btn-primary"> Create </a>
            </div>
        </div>

        <div class="mr-30">
            <table class="table table-striped mx-15 mb-0 ">
                <tr class="bg-fbfcfd color-AFAFAF">
                    <th width="3%" class="text-center"><input type="checkbox" class="js-cb-toggle-all" data-targets="target_cbs_id"></th>
                    <th width="7%" class="text-center" >{{ __('THUMBNAIL') }}</th>
                    <th width="30%" class=""><a href="{{ $myOrder['title'] ?? '' }}">{{ __('TITLE') }}<i class="{{ $mySortIcon['iconTitle'] ?? '' }}"></i></a></th>
                    <th width="30%" class=""><a href="{{ $myOrder['brand'] ?? '' }}">{{ __('Brand') }}<i class="{{ $mySortIcon['iconTitle'] ?? '' }}"></i></a></th>
                    <th width="10%" class="text-center"><a href="{{ $myOrder['slug'] ?? '' }}">{{ __('CREATED_BY ') }}<i class="{{ $mySortIcon['iconSlug'] ?? '' }}"></i></a></th>
                    <th width="15%" class="text-center">{{ __('ACTION') }}</th>
                </tr>
                @if ($products->isNotEmpty())
                    @foreach ($products as $product)
                        @php
                            $productImage = $product->images->first();
                        @endphp
                        <tr class="child">
                            <td class="text-center vertical-align-middle"><input type="checkbox" id="target_cbs_id" value="{{ $product->id }}"></td>
                            <td class="text-center vertical-align-middle"><img src="{{$productImage ? asset('storage/' .$productImage->path) : 'https://via.placeholder.com/600'}}" alt="#" width="50px"></td>
                            <td class=" vertical-align-middle"><a href="#" title="{{ $product->name }}">{{ $product->name }}</a></td>
                            <td class="text-center vertical-align-middle">{{ $product->brand ? $product->brand->name : '' }}</td>
                            <td class="text-center vertical-align-middle">{{ $product->created_at->format('Y-m-d') }}</td>
                            <td class="text-center vertical-align-middle" style="display: flex">
                                <a href="{{route('admin.product.edit', $product->id)}}" class="btn btn-sm btn-primary">{{ __('Edit') }}</a>
                                <form onsubmit="return confirm('Do you really want to delete?');" action="{{ route('admin.product.delete',$product->id) }}" method="POST" style="margin-left:10px">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"  href="" class="btn btn-sm btn-danger">{{ __('Delete') }}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <td  colspan="7" class="text-center vertical-align-middle"> No Product</td>
                @endif

            </table>

            <div class="row mr-a30 ml-0">
                <div class="col-sm-6">
                    <span class="dt-length-records">
                    <i class="fa fa-globe"></i> <span class="badge badge-secondary bold badge-dt">{{ $products->count() }}</span> <span class="hidden-xs">{{ __('records') }}</span>
                    </span>
                </div>
                <div class="col-sm-6 display-flex justify-content-end">{{ $products->links() }}</div>
            </div>
        </div>

        </div>

    </div>



@endsection

