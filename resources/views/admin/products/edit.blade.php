@extends('admin.layouts.layout')
@section('title', 'Add Product')
@section('content')
<div class="content-wrapper" style="min-height: calc(100vh - 200px)">
    <section class="content-header">
        <h1>{{ __('Create Post') }} </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> {{ __('Dashboard') }}</a></li>
            <li><a href="{{ url('/products') }}">{{ __('Products') }} </a></li>
            <li class="active">{{ __('Create') }}</li>
        </ol>
    </section>
    <form action="{{ route('admin.product.update', $product->id) }}" method="POST" enctype="multipart/form-data" class="mx-15 mt-20">
        @csrf
        @method('POST')
        <div class="row">
            <div class="col-md-9">
                <div class="main-form bg-white pxy-15">
                    <div class="form-body row">
                        <div class="form-group col-md-12">
                            @include('admin.components.input.text',[
                            'class' => 'form-control',
                            'label' => __('Name'),
                            'name' => 'name',
                            'required' => true,
                            'default' => $product->name,
                            'placeholder' => __('Name'),
                            ])
                        </div>
                        <div class="form-group col-md-12">
                            @include('admin.components.input.number',[
                            'class' => 'form-control',
                            'label' => __('Price'),
                            'name' => 'price',
                            'required' => true,
                            'default' => $product->price,
                            'placeholder' => __('Price'),
                            ])
                        </div>
                        <div class="form-group col-md-12">
                            @include('admin.components.textarea.textarea',[
                                'class' => 'form-control',
                                'label' => __('Description'),
                                'name'  => 'description',
                                'required' => true,
                                'default' => $product->description,
                                'placeholder' => __('Short description')
                            ])
                        </div>

                        <div class="form-group col-md-12">
                            @include('admin.components.textarea.textarea',[
                                'class' => 'form-control',
                                'label' => __('delivery'),
                                'name'  => 'delivery',
                                'required' => false,
                                'default' => $product->delivery,
                                'placeholder' => __('delivery')
                            ])
                        </div>
                        <div class="form-group col-md-12">
                            @include('admin.components.textarea.textarea',[
                                'class' => 'form-control',
                                'label' => __('warranty'),
                                'name'  => 'warranty_info',
                                'required' => false,
                                'default' => $product->warranty_info,
                                'placeholder' => __('warranty')
                            ])
                        </div>

                    </div>
                </div>
                <p></p>
            </div>
            <div class="col-md-3 right-sidebar">
                <div class="bg-white widget">
                    <div class="widget-title">
                        <div class="btn-set">
                            <button type="submit" name="submit" value="save" class="btn btn-info">
                                <i class="fa fa-save"></i> {{ __(' Save') }}
                            </button>
                        </div>
                    </div>
                    <p></p>
                    <div class="widget-body">
                        @include('admin.components.input.file',[
                        'class' => 'form-control',
                        'label' => __('Images'),
                        'img' => $product->images->first() ? asset('storage/' .$product->images->first()->path): 'https://via.placeholder.com/300',
                        'name' => 'images[]',
                        'default' => '',
                        ])
                    </div>
                    <div class="widget-body">
                        @include('admin.components.select.option',[
                            'class' => 'form-control',
                            'label' => __('Brand'),
                            'name' => 'brand_id',
                            'required' => true,
                            'default' => $product->brand_id,
                            'options' => $brands->pluck('name','id')->toArray(),
                        ])
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>
@endsection
