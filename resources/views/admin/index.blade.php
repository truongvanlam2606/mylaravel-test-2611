@extends('admin.layouts.layout')
@section('title', 'Dashboard')
@push('head')
@endpush
@section('content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">

          </div>
          <!-- /.row -->
          <!-- Main row -->
          <div class="row">

          </div>
          <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <div class="control-sidebar-bg"></div>
    </div>
@endsection
@push('scripts')
@endpush
