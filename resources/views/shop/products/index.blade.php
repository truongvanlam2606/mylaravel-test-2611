<div class="page-products">
    <div class="row products">

        @if ($products->isNotEmpty())
            @foreach ( $products as $product)
                @include('shop.products.product-item', ['product' => $product])
            @endforeach
        @endif

    </div>

    <div class="section paginate">
        {{$products->links()}}
    </div>
</div>
