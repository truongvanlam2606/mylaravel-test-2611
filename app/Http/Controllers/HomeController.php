<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    const PAGINATE = 9;

    public function index(Request $request){

        $products = Product::with('images')
                        ->orderBy('created_at', 'desc')
                        ->paginate(self::PAGINATE);

        return view('shop.index', compact('products'));
    }


}
