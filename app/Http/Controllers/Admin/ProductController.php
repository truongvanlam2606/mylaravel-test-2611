<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    const PAGINATE = 9;

    public function index(Request $request){

        $products = Product::with('images','brand')
                        ->orderBy('created_at', 'desc')
                        ->paginate(self::PAGINATE);

        return view('admin.products.index', compact('products'));
    }

    public function delete($id){

        $product = Product::find($id);

        if(!$product){
            return redirect()->back();
        }

        $product->delete();

        return redirect()->route('admin.product.index')->with(['Delete'=>'Delete Successfully','Alert'=>'Delete']);
    }

    public function create(){

        $product = new Product();
        $brands = Brand::get();
        return view('admin.products.create', compact('product','brands'));
    }
    public function store(Request $request){

        $data = $request->only([
            'name', 'price', 'description', 'delivery', 'warranty_info', 'brand_id',
        ]);

        $product = Product::create($data);

        if(!$product){
            return redirect()->brack()->with(['Create'=>'Create Faild','Alert'=>'Create']);
        }

        // Create Images;
        $images = $this->saveImages($request->file('images', []), $product->id);
        if(!empty($images)){
           $product->images()->createMany($images);
        }

        return redirect()->route('admin.product.index')->with(['Create'=>'Create Successfully','Alert'=>'Create']);
    }

    private function saveImages(array $images, $productId):array {

        $dataImages = [];
        foreach($images as $image){
            if($image){
                try {
                    $fileName = $image->getClientOriginalName();
                    $path = $image->store('images');
                    if($path){
                        $dataImages[] = [
                            'name' => $fileName,
                            'path' => $path,
                            'product_id' => $productId,
                        ];
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
        }

        return $dataImages;
    }

    public function edit($id){

        $product = Product::find($id);
        $brands = Brand::get();

        if(!$product){
            return redirect()->back();
        }

        return view('admin.products.edit', compact('product','brands'));
    }

    public function update($id, Request $request){

        $product = Product::find($id);

        if(!$product){
            return redirect()->back();
        }

        $data = $request->only([
            'name', 'price', 'description', 'delivery', 'warranty_info', 'brand_id',
        ]);
        $product->update($data);

        $images = $this->saveImages($request->file('images', []), $product->id);
        if(!empty($images)){
            $product->images()->delete();
            $product->images()->createMany($images);
        }

        return redirect()->route('admin.product.index')->with(['Delete'=>'Delete Successfully','Alert'=>'Delete']);
    }


}
