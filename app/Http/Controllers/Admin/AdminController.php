<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    const PAGINATE = 9;

    public function index(Request $request){

        $products = Product::with('images')->paginate(self::PAGINATE);

        return view('admin.index', compact('products'));
    }


}
