<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class CartController extends Controller
{

    const PAGINATE = 9;

    public function addProductToCart($productId, Request $request){

        $data = $request->all();
        $qty = $data['qty'] ?? 1;
        $product = Product::find($productId);
        if(!$product){
            return response()->json([
                'error' => true,
                'message' => 'Not found product'
            ]);
        }
        $sessionId = (string) Str::uuid();
        $cart = Session::get('cart');
        if($cart){
            $checkCartItem = array_filter($cart, function($item) use($productId){
                return $item['product_id'] == $productId;
            });

            if(empty($checkCartItem)){
                $cart[] = [
                    'session_id' => $sessionId,
                    'product_id' => $product->id,
                    'name' => $product->name,
                    'price' => $product->price,
                    'qty' => $qty,
                    'image' => asset('storage/' .($product->images()->first() ? $product->images()->first()->path : 'https://picsum.photos/600')),
                ];
            }else{

                foreach($cart as $index => $carItem){
                    if($carItem['product_id'] == $product->id){
                        $cart[$index]['qty'] += $qty;
                    }
                }
            }
        }else{
            $cart[] = [
                'session_id' => $sessionId,
                'product_id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'qty' => $qty,
                'image' => asset('storage/' .($product->images()->first() ? $product->images()->first()->path : 'https://picsum.photos/600')),
            ];
        }
        Session::put('cart', $cart);
        Session::save();

        return response()->json([
                'success' => true,
                'message' => 'ok',
                'cart' => $cart
        ]);

    }
    public function removeProductToCart($productId, Request $request){

        $data = $request->all();
        $qty = $data['qty'] ?? 1;
        $product = Product::find($productId);
        if(!$product){
            return response()->json([
                'error' => true,
                'message' => 'Not found product'
            ]);
        }
        $cart = Session::get('cart');


        if(!$cart){
            return response()->json([
                'success' => true,
                'message' => 'Cart does not exist',
                'cart' => $cart
        ]);
        }

        foreach($cart as $index => $carItem){
            if($carItem['product_id'] == $product->id){
               unset($cart[$index]);
            }
        }

        Session::put('cart', $cart);
        Session::save();

        return response()->json([
                'success' => true,
                'message' => 'ok',
                'cart' => $cart
        ]);

    }

    public function getCart(){

        $cart = Session::get('cart');
        return response()->json([
                'success' => true,
                'message' => 'ok',
                'cart' => $cart,
        ]);

    }


}
