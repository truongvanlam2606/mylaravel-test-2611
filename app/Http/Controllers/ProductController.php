<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    const PAGINATE = 9;

    public function view($id){

        $product = Product::findOrFail($id);
        return view('shop.products.detail', compact('product'));

    }


}
