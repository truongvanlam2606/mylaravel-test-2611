<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function brand(){

        return $this->hasOne(Brand::class);
    }

    public function images(){

        return $this->hasMany(ProductImage::class);
    }

    public function users(){

        return $this->hasMany(User::class);
    }

}
