<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'name', 'path', 'from_seed'
    ];

    public function product(){

        return $this->hasOne(Product::class);
    }
}
