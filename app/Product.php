<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'name', 'price', 'description', 'delivery', 'warranty_info', 'brand_id'
    ];

    public function brand(){

        return $this->belongsTo(Brand::class);
    }

    public function images(){

        return $this->hasMany(ProductImage::class);
    }

}
