<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group([
    'namespace' => 'Admin',
    'middleware' => ['auth'],
], function () {
    Route::get('/','AdminController@index')->name('admin.index');

    Route::group([
        'prefix' => 'products',
    ], function () {
        Route::get('/','ProductController@index')->name('admin.product.index');
        Route::delete('/{id}','ProductController@delete')->name('admin.product.delete');
        Route::get('/create','ProductController@create')->name('admin.product.create');
        Route::post('/store','ProductController@store')->name('admin.product.store');
        Route::get('/edit/{id}','ProductController@edit')->name('admin.product.edit');
        Route::post('/update/{id}','ProductController@update')->name('admin.product.update');
    });

});


