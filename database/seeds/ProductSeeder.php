<?php

use App\Product;
use App\ProductImage;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 10)->create()->each(function ($product) {
            $product->images()->saveMany(factory(ProductImage::class, rand(1,2))->make());
        });
    }
}
