<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'Liam Truong',
            'email' => 'admin@admin.com',
            'email_verified_at' => Carbon::now()->toDateTimeString(),
            'password' => bcrypt('admin12345'),
            'role_id' => 1,
        ]);
    }
}
