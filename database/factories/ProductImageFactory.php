<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\ProductImage;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ProductImage::class, function (Faker $faker) {

    // $url = $faker->imageUrl(640,480);
    $url = "https://picsum.photos/640/480";

    $filename = Str::slug($faker->name) .'.jpg';
    $path =  'images/' .$filename;

    $arrContextOptions=array(
        "ssl"=>array(
              "verify_peer"=>false,
              "verify_peer_name"=>false,
          ),
      );

    $loop = 1;
    do {
        try {
            $contents = file_get_contents($url, false, stream_context_create($arrContextOptions));
            $storage = Storage::put( $path, $contents);
        } catch (\Throwable $th) {
            Log::error('Error get content Image: ' .$th->getMessage());
            $loop++;
        }
    } while ($loop > 1 &&  $loop < 3);

    return [
        'name' => $faker->name,
        'path' => $path,
        'from_seed' => true
    ];

});
