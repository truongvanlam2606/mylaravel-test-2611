## Project Laravel Test

## Installation Instructions

1. copy .env.example

2. Create database mysql.

3. install composer
- composer install

3. Run data

- php artisan migrate
- php artisan db:seed

4. Optimize

- php artisan storage:link
- php artisan optimize:clear

5. start server

- php artisan serve

 * NOTE : Run php artisan server => error path image 
 Please configure the virtual domain to avoid the error of the image path

## Login admin

- path : /admin
- Account:
    + email: admin@admin.com
    + pass:  admin12345


## VIDEO Demo

include folder DEMO/Home - Google Chrome 2021-11-27 21-08-53.pm4


